﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace htowers
{
    class Program
    {
        public static void Main()
        {
            int n = 0;
            Console.Write("Введите количество дисков: ");
            try
            {
                n = int.Parse(Console.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.Message);
                Environment.Exit(-1);
            }

            Console.WriteLine($"Решение для {n} дисков:");
            Htow(n, 'A', 'B', 'C');
            Console.WriteLine("\nГотово!");
        }

        public static void Htow(int k, char a, char b, char c)
        {
            if (k > 1)
            {
                Htow(k - 1, a, c, b);
            } 
            Console.WriteLine($"Переложить диск из {a} в {b}");
            if (k > 1)
            {
                Htow(k - 1, c, b, a);
            }
        }
    }
}